Introduction
============

This is a small project to compare Hibernate/Spring JDBC template insertion performance.


Pre-requisites
--------------

This project requires a locally configured MySQL instance with the following connection parameters:

url = jdbc:mysql://localhost/testdb
user = testdb
pass = testdb

The results
-----------

These are typical results of running a very simple insert.

Hibernate runtime 3561 milliseconds
Template runtime 45917 milliseconds
Batch Template runtime 2950 milliseconds

Conclusion
----------

It seems that batch template insertion is 20% faster than Hibernate for simple insertion.

Not a huge difference. Certainly much less than I would have expected. 
