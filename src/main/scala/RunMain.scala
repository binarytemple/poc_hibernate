import collection.immutable.IndexedSeq
import com.mysql.jdbc.jdbc2.optional.MysqlDataSource


import ie.hunt.Person
import java.sql.{PreparedStatement, Connection}
import javax.persistence.{EntityTransaction, EntityManager, Persistence}
import org.apache.commons.lang.time.StopWatch

import org.springframework.jdbc.core.{ParameterizedPreparedStatementSetter, JdbcTemplate, PreparedStatementCreator}


/**
 * User: bryan
 * Date: 9/17/12
 * Time: 2:03 PM
 */

class RunMain(jdbcTemplate: JdbcTemplate) {

  def insert(p: Person) = {
    jdbcTemplate.update(
      new PreparedStatementCreator {
        def createPreparedStatement(con: Connection): PreparedStatement = {
          val ps = con.prepareStatement(insertSql)
          ps.setString(1, "dave")
          ps
        }
      })
  }


  val insertSql =
    """
      | INSERT INTO person
      | (
      | title
      | )
      | VALUES
      | (?)
      | """.stripMargin

  def batchInsert(persons: List[Person]) = {
              import scala.collection.JavaConversions.asJavaCollection
    jdbcTemplate.batchUpdate(insertSql,
      persons ,
      1000,
      new ParameterizedPreparedStatementSetter[Person] {
        def setValues(ps: PreparedStatement, argument: Person) {
          var field: Int = 1
          ps.setString(1,argument.getTitle)
        }
      }
    )
  }
}

object RunMain extends App {

  val RECORDS_TO_INSERT: Int = 5000
  val emf = Persistence.createEntityManagerFactory("example");
  var em: EntityManager = emf.createEntityManager()


  var source: MysqlDataSource = new MysqlDataSource
  source.setUrl("jdbc:mysql://localhost/testdb")
  source.setUser("testdb")
  source.setPassword("testdb")


  var template: JdbcTemplate = new JdbcTemplate()
  template.setDataSource(source)

  var main: RunMain = new RunMain(template)


  var watch: StopWatch = new StopWatch
  watch.start()
  var t: EntityTransaction = em.getTransaction
  t.begin()

  Range(1, RECORDS_TO_INSERT).grouped(1000).foreach(i => {
    //println("batching");
    i.foreach(j => {
      //println(j)
      val p = new Person
      p.setTitle("dave")
      em.persist(p)
    }
    )
    em.flush()
  })

  //  var q: TypedQuery[Person] = em.createQuery("FROM Person", classOf[Person])
  //  var res: util.List[Person] = q.getResultList
  //  import scala.collection.JavaConversions._
  //  res.toList.foreach(p => println(p))
  t.commit
  watch.split
  var split: Long = watch.getSplitTime
  println("Hibernate runtime %s milliseconds".format(split))

  var connection: Connection = template.getDataSource.getConnection
  connection.setTransactionIsolation(Connection.TRANSACTION_READ_UNCOMMITTED)
  connection.setAutoCommit(false)

  Range(1, RECORDS_TO_INSERT).grouped(1000).foreach(i => {
    //println("batching");

    i.foreach(j => {
      //println(j)
      val p = new Person
      p.setId(null)
      p.setTitle("dave2")
      main.insert(p)
    }
    )
    connection.commit()
  })

  watch.split()
  var split2: Long = watch.getSplitTime

  println("Template runtime %s milliseconds".format((split2 - split)))


  Range(1, RECORDS_TO_INSERT).grouped(1000).foreach(i => {
    //println("batching");

    val persons = i.map(j => {

      val p = new Person
      p.setId(null)
      p.setTitle("dave2")
      p
    }
    ).toList
    main.batchInsert(persons)
    connection.commit()
  })

  watch.split()
  var split3: Long = watch.getSplitTime
  println("Batch Template runtime %s milliseconds".format((split3 - split2)))

}